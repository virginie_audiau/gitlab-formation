# Formation Gitlab (Orsys)

## Infos

* Nom : GitLab CI/CD, maîtriser la gestion du cycle de vie de vos développements logiciels – N°41063 / Option 1 - réf : GLNCV1
* Intervenant : Monsieur PLANTROU DAVID

## Liens

* Gitlab : https://gitlab.com/dashboard/projects
* Git cheat sheet : https://about.gitlab.com/images/press/git-cheat-sheet.pdf
* Documentation Gitlab : https://docs.gitlab.com/ee/ci/
* Gitlab Markdown : https://docs.gitlab.com/ee/user/markdown.html#gitlab-flavored-markdown
* __Learn Git Branching__  https://learngitbranching.js.org/?locale=fr_FR

## Premiers commit
Commit sur branch1


## Commandes usuelles
### Infos
```
git show --pretty
git show --pretty= --name-only <commit>
git log
git log --all
git log -n 1 # last commit
git log --since=2.weeks
git log --graph
git log --decorate
git log --pretty=format:"%h - %ar - %an : %Cgreen %s"

git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all
```

### Branches
```
git branch # List branches
git branch newbranch

git checkout branch, revision, HEAD, HEAD~2, HEAD^^...

git checkout newbranch # remplacé par git switch
git switch <branch>
```
### Divers
```
# Modifier le dernier commit sans changer son commentaire (mais génère un nouvel revision id)
git commit --amend --no-edit

# Reviens de 2 commits sans modifier les fichiers ni le cache, peut être utile pour fusionner plusieurs commits en un seul
git reset --soft HEAD~2
git reset --mixed HEAD~2 # pareil mais revert cache
git reset --hard HEAD~2 # pareil mais revert cache et les fichiers, la situation actuelle est perdue

git diff <file>
git diff --cached <file>
git diff HEAD <file>
git diff HEAD~2 <file>

# interactive commits reviewer
git rebase -i master~4

```

### Aliases
```
git config --global alias.logall "log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"

git config --global alias.oops "commit --amend --no-edit"
```

### Graphical interface
```
gitk (à lancer en cli)
https://www.sourcetreeapp.com/ (gratuit)
gitkraken (payant)
```
