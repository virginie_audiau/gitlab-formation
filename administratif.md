# Infos

* Nom : GitLab CI/CD, maîtriser la gestion du cycle de vie de vos développements logiciels – N°41063 / Option 1 - réf : GLNCV1
* Intervenant : Monsieur PLANTROU DAVID

# Liens

* Teams : https://teams.microsoft.com/l/meetup-join/19%3ameeting_MzAzMzk3OTUtOGU4Ny00NzM1LWExYzgtMWI1NDQyMzYxODAy%40thread.v2/0?context=%7b%22Tid%22%3a%22020dbb87-3e10-4d6f-8252-d7ad37b87855%22%2c%22Oid%22%3a%22ad57a8d0-ba0d-4829-8062-959567aa2814%22%7d

* Emargement : https://emargement.orsys.fr/Home/Emargement/0x01000000C149CA5F1A986A3359C4FDA30A5592B1C8B98726FCA78662

* Evaluation : https://eval.orsys.fr/

* Hotline : +33 (0)9.71.16.01.15 (hotline@orsys.fr)
